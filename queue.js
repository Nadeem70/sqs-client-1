const AWS = require("aws-sdk");
const sqsMove = require("@sagacify/sqs-move");
const express = require("express");
const app = express();
var cron = require("node-cron");
const fs = require("fs");

fs.readFile("./config.json", (error, data) => {
  if (error) {
    throw error;
  }
  const { noOfMinutes, fromQueueUrl, toQueueUrl } = JSON.parse(data);

  const sqsInstance = new AWS.SQS({
    region: "ap-southeast-1"
  });

  var schedule = "*/" + String(noOfMinutes) + " * * * *";

  cron.schedule(schedule, async () => {
    const { movedCount } = await sqsMove(
      sqsInstance,
      fromQueueUrl,
      toQueueUrl,
      {
        batchSize: 1,
        includes: null,
        excludes: null,
        transformBody: (body, messageAttributes) => {
          console.log("message moved: " + body);
          return body;
        },
        transformMessageAttributes: null,
        json: false,
      }
    );
    console.log("moved count is: " + movedCount);
  });
});

app.listen(3000, function (err, data) {
  if (err) {
    console.log("error");
  } else {
    console.log("running on port 3000");
  }
});
